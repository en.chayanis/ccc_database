module test3

go 1.16

require (
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/gorilla/handlers v1.5.1 // indirect
	github.com/gorilla/sessions v1.2.1 // indirect
	github.com/julienschmidt/httprouter v1.3.0 // indirect
	github.com/pelletier/go-toml v1.9.4 // indirect
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292 // indirect
)
