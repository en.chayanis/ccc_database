package main

import (
	"bufio"
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"io"
	"io/ioutil"
	"strconv"
	"strings"

	//"errors"
	"fmt"
	"log"
	"net/http"

	"golang.org/x/crypto/bcrypt"

	"os"
	"path/filepath"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/handlers"
	"github.com/gorilla/sessions"
	"github.com/julienschmidt/httprouter"
)

var store = sessions.NewCookieStore([]byte("super-secret"))

//========================struct[POST]========================//

type Log_in struct {
	Id_user  int    `json:"id_user"`
	E_mail   string `json:"e_mail"`
	Password string `json:"password"`
	Up_date  string `json:"date"`
	Up_time  string `json:"time"`
	Hash     string `json:"hash"`
}

type Worker struct {
	Id_worker      int    `json:"id_worker"`
	Id_manager     int    `json:"id_manager"`
	Id_bubble      int    `json:"id_bubble"`
	Name_worker    string `json:"name_worker"`
	Tel_worker     string `json:"tel_worker"`
	Address_worker string `json:"address_worker"`
	Date_worker    string `json:"date"`
	Time_worker    string `json:"time"`
}

type GetWorker struct {
	Id_worker      int    `json:"id_worker"`
	Id_manager     int    `json:"id_manager"`
	Id_bubble      int    `json:"id_bubble"`
	Name_worker    string `json:"name_worker"`
	Tel_worker     string `json:"tel_worker"`
	Address_worker string `json:"address_worker"`
	Date_worker    string `json:"date"`
	Time_worker    string `json:"time"`
	Name_bubble    string `json:"name_bubble"`
}

type Bubble struct {
	Id_bub     int    `json:"id_bub"`
	Id_mng     int    `json:"id_mng"`
	Name_bub   string `json:"name_bub"`
	Detail_bub string `json:"detail_bub"`
	Date_bub   string `json:"date"`
	Time_bub   string `json:"time"`
}

type Class_result struct {
	Id_result   int    `json:"id_result"`
	Name_result string `json:"name_result"`
}

type Notify struct {
	Date_lab              string `json:"date"`
	Time_lab              string `json:"time"`
	Name_result_person    string `json:"name_result_person"`
	Result_lab_person     int    `json:"result_person"`
	Name_result_hardhat   string `json:"name_result_hardhat "`
	Result_lab_hardhat    int    `json:"result_hardhat "`
	Name_result_nohardhat string `json:"name_result_nohardhat"`
	Result_lab_nohardhat  int    `json:"result_nohardhat"`
}

type Summary struct {
	Date_lab              string `json:"date"`
	Name_result_person    string `json:"name_result_person"`
	Result_sum_person     int    `json:"result_person"`
	Name_result_hardhat   string `json:"name_result_hardhat "`
	Result_sum_hardhat    int    `json:"result_hardhat "`
	Name_result_nohardhat string `json:"name_result_nohardhat"`
	Result_sum_nohardhat  int    `json:"result_nohardhat"`
}

type Selected_bubble struct {
	Id_mng_sb  int `json:"id_mng"`
	Id_site_sb int `json:"id_site"`
	Id_bub_sb  int `json:"id_bub"`
}

type Selected_bubble_new struct {
	Date       string `json:"date"`
	Detail_bub string `json:"detail_bub"`
	Id_bub     int    `json:"id_bub"`
	Id_mng     int    `json:"id_mng"`
	Id_site    int    `json:"id_site"`
	Name_bub   string `json:"name_bub"`
	Time       string `json:"time"`
}

type Ssop struct {
	Id_ssop     int    `json:"id_ssop"`
	Description string `json:"description"`
	Score       int    `json:"score"`
}

type Ssop_group struct {
	Id_group   int    `json:"id_group"`
	Name_group string `json:"name_group"`
}

type Site_test struct {
	Id_site      int    `json:"id_site"`
	Id_user      int    `json:"id_user"`
	Name_site    string `json:"name_site"`
	Address_site string `json:"address_site"`
	Manager_site string `json:"manager_site"`
	Manager_tel  string `json:"manager_tel"`
	File_name    string `json:"file_name"`
	File_path    string `json:"file_path"`
	Upd_date     string `json:"upd_date"`
	Upd_time     string `json:"upd_time"`
}

// Report SSOP
type Selected_group struct {
	Id_group_selected int    `json:"id_g"`
	Id_ssop_selected  int    `json:"id_s"`
	Id_site_sg        int    `json:"id_site_sg"`
	Id_user_sg        int    `json:"id_user_sg"`
	N_group           string `json:"n_g"`
	N_ssop            string `json:"n_s"`
	Score             int    `json:"score_s"`
}

//========================functions[POST]========================//

func Index(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	ID := prm.ByName("id")
	siteId := prm.ByName("siteId")
	var site Site_test
	site.Id_site, _ = strconv.Atoi(siteId)
	site.Id_user, _ = strconv.Atoi(ID)
	fmt.Println(site)
}

func NewLogin() *Log_in {
	return new(Log_in)
}

func register(w http.ResponseWriter, req *http.Request, _ httprouter.Params) {
	body := req.Body
	login := NewLogin()
	err := json.NewDecoder(body).Decode(login)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		fmt.Println("json")
		return
	}
	defer body.Close()
	err = login.CreateNewUser()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		fmt.Println("login")
		return
	}
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w)
}

func (data *Log_in) CreateNewUser() error {
	db, err := GetDB()
	if err != nil {
		return err
	}
	defer db.Close()
	var hash []byte
	hash, err = bcrypt.GenerateFromPassword([]byte(data.Password), bcrypt.DefaultCost)
	if err != nil {
		fmt.Println("bcrypt err:", err)
	}
	fmt.Println("hash:", hash)
	fmt.Println("string(hash):", string(hash))
	_, err = db.Exec(`INSERT INTO log_in VALUES (NULL,?,?,SYSDATE(),time(NOW()),?)`, data.E_mail, data.Password, hash)
	if err != nil {
		return err
	}
	return nil
}

type User struct {
	Id_user int `json:"id_user"`
}

func NewUser() *User {
	return new(User)
}

func login(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	body := req.Body
	var e_mail Log_in
	err := json.NewDecoder(body).Decode(&e_mail)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	db, err1 := GetDB()
	if err1 != nil {
		return
	}
	defer db.Close()
	var hash string
	stmt := "SELECT Hash FROM log_in WHERE E_mail = ?"
	row := db.QueryRow(stmt, e_mail.E_mail)
	err5 := row.Scan(&hash)
	fmt.Println("hash from db:", hash)
	if err5 != nil {
		fmt.Println("error selecting Hash in db by email")
	}
	var id_user int
	id := "SELECT Id_user FROM log_in WHERE E_mail = ?"
	rows := db.QueryRow(id, e_mail.E_mail)
	errs := rows.Scan(&id_user)
	if errs != nil {
		fmt.Println("error selecting Id_user in db by email")
	}

	var mPassword string
	stmt2 := "SELECT Password FROM log_in WHERE E_mail = ?"
	row2 := db.QueryRow(stmt2, e_mail.E_mail)
	err6 := row2.Scan(&mPassword)
	fmt.Println("password from db:", mPassword)
	if err6 != nil {
		fmt.Println("error selecting password in db by email")

	}

	if mPassword == e_mail.Password {
		session, _ := store.Get(req, "session")
		session.Values["userID"] = id_user
		session.Save(req, w)
		fmt.Println("login!!")
		id_user := session.Values["userID"]
		fmt.Println("ok:", id_user)
		db, err := GetDB()

		if err != nil {
			return
		}
		defer db.Close()

		if mId, ok := id_user.(int); ok {

			fmt.Println("ok2:", mId)
			user := User{Id_user: mId}
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusCreated)
			json.NewEncoder(w).Encode(user)
			return
		} else {
			fmt.Println("ok3:", id_user.(int))
			user := User{}
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusCreated)
			json.NewEncoder(w).Encode(user)
			return
		}
	}
	fmt.Println("incorrect password")
}

func aboutHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Println("*****aboutHandler running*****")
}

func logout(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Println("*****logoutHandler running*****")
	session, _ := store.Get(r, "session")
	delete(session.Values, "userID")
	session.Save(r, w)
	fmt.Println("logout!!")
}

func NewBubble() *Bubble {
	return new(Bubble)
}

func add_bubble(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	fmt.Println("*****addsite Handler running*****")
	ID := prm.ByName("id")
	fmt.Println("ok:", ID)
	w.Header().Set("Content-Type", "application/json")
	body := req.Body
	bubble := NewBubble()
	error := json.NewDecoder(body).Decode(bubble)
	if error != nil {
		http.Error(w, error.Error(), http.StatusInternalServerError)
		return
	}
	defer body.Close()
	error_check := bubble.CreateNewBubble(ID)
	if error_check != nil {
		http.Error(w, error_check.Error(), http.StatusInternalServerError)
		return
	}
	if error_check == nil {
		fmt.Println("add bubble complete")
		return
	}
}

func (data *Bubble) CreateNewBubble(ID string) error {
	db, err := GetDB()
	if err != nil {
		return err
	}
	defer db.Close()
	_, err = db.Exec(`INSERT INTO bubble VALUES (NULL,?,?,?,SYSDATE(),time(NOW()))`, ID, data.Name_bub, data.Detail_bub)
	if err != nil {
		return err
	}
	return nil
}

func NewWorker() *Worker {
	return new(Worker)
}

func add_worker(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	fmt.Println("*****addsite Handler running*****")
	ID := prm.ByName("id")
	fmt.Println("ok:", ID)
	w.Header().Set("Content-Type", "application/json")
	body := req.Body
	worker := NewWorker()
	error := json.NewDecoder(body).Decode(worker)
	if error != nil {
		http.Error(w, error.Error(), http.StatusInternalServerError)
		return
	}
	defer body.Close()
	error_check := worker.CreateNewWorker(ID)
	if error_check != nil {
		http.Error(w, error.Error(), http.StatusInternalServerError)
		return
	}
	if error_check == nil {
		fmt.Println("add worker complete")
		return
	}
}

func (data *Worker) CreateNewWorker(ID string) error {
	db, err := GetDB()
	if err != nil {
		return err
	}
	defer db.Close()
	_, err = db.Exec(`INSERT INTO worker VALUES (NULL,?,?,?,?,?,SYSDATE(),time(NOW()))`, ID, data.Id_bubble, data.Name_worker, data.Tel_worker, data.Address_worker)
	if err != nil {
		return err
	}
	return nil
}

func NewSelectBubble() *Selected_bubble {
	return new(Selected_bubble)
}

func selected_bubble(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	fmt.Println("*****selected_bubble Handler running*****")
	w.Header().Set("Content-Type", "application/json")
	b, err := ioutil.ReadAll(req.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	var msg []Selected_bubble_new
	err = json.Unmarshal(b, &msg)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	fmt.Println(msg)
	db, _ := GetDB()
	defer db.Close()
	query := "INSERT INTO selected_bubble VALUES"
	var vals []interface{}

	for _, row := range msg {
		fmt.Println(row)
		query += "(NULL,?,?,?,SYSDATE(),time(NOW())) ,"          //<--- check param
		vals = append(vals, row.Id_mng, row.Id_site, row.Id_bub) //<---  check param
	}
	fmt.Println(vals)
	_, err = db.Exec(strings.TrimSuffix(query, " ,"), vals...) //<---check command insert
	fmt.Println(err)
}

func (data *Selected_bubble) CreateNewSelectBubble(c []Selected_bubble) error {
	db, err := GetDB()
	if err != nil {
		return err
	}
	defer db.Close()
	sqlStr := "INSERT INTO selected_bubble VALUES"
	var vals []interface{}

	for _, row := range c {
		sqlStr += "(?,?,?,SYSDATE(),time(NOW())),"
		vals = append(vals, row.Id_mng_sb, row.Id_site_sb, row.Id_bub_sb)
	}
	fmt.Println(vals)
	_, err1 := db.Exec(sqlStr, vals)
	return err1
}

func NewADDSite() *Site_test {
	return new(Site_test)
}

func uploadsite(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	file, handler, err := req.FormFile("file")
	if err != nil {
		fmt.Println("Error Retrieve file from form form-data")
		fmt.Println(err)
		return
	}
	defer file.Close()
	fmt.Printf("Uploaded File: %+v\n", handler.Filename)
	fmt.Printf("File Size: %+v\n", handler.Size)
	fmt.Printf("MIME Header: %+v\n", handler.Header)
	ID := prm.ByName("id")
	fmt.Println("ok:", ID)
	w.Header().Set("Content-Type", "multipart/form-data")
	body := req.Body
	site := NewADDSite()
	/** **/
	site.File_name = req.FormValue("file_name")
	site.Name_site = req.FormValue("name_site")
	site.Address_site = req.FormValue("address_site")
	site.Manager_site = req.FormValue("manager_site")
	site.Manager_tel = req.FormValue("manager_tel")
	fmt.Printf(site.File_name)
	fmt.Printf(site.Name_site)
	fmt.Printf(site.Address_site)
	fmt.Printf(site.Manager_site)
	fmt.Printf(site.Manager_tel)
	/** **/
	// error := json.NewDecoder(body).Decode(site)
	// if error != nil {
	// 	http.Error(w, error.Error(), http.StatusInternalServerError)
	// 	return
	// }
	defer body.Close()
	dst, err := os.Create(filepath.Join("/home/mew/golang-api/images", filepath.Base(handler.Filename)))
	if err != nil {
		fmt.Println(err)
		return
	}
	defer dst.Close()
	if _, err = io.Copy(dst, file); err != nil {
		fmt.Println(err)
		return
	}

	// Copy the uploaded file to the created file on the filesystem
	if _, err := io.Copy(dst, file); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// create a new buffer base on file size
	fInfo, _ := dst.Stat()
	var size int64 = fInfo.Size()
	buf := make([]byte, size)

	// read file content into buffer
	fReader := bufio.NewReader(dst)
	fReader.Read(buf)
	//convert the buffer bytes to base64 string - use buf.Bytes() for new image
	imgBase64Str := base64.StdEncoding.EncodeToString(buf)

	//Decoding
	sDec, _ := base64.StdEncoding.DecodeString(imgBase64Str)
	fmt.Println(sDec)
	filepath := `/golang-api/images/` + handler.Filename
	fmt.Println(filepath)
	db, err := GetDB()
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()
	error_check := site.CreateNewUpSite(ID, handler.Filename, filepath)
	if error_check != nil {
		http.Error(w, error_check.Error(), http.StatusInternalServerError)
		return
	}
	if error_check == nil {
		fmt.Println("upload site complete")
		return
	}

}

func (data *Site_test) CreateNewUpSite(ID string, filename string, filepath string) error {
	db, err := GetDB()
	if err != nil {
		return err
	}
	defer db.Close()
	_, err = db.Exec(`INSERT INTO site_test VALUES (NULL,?,?,?,?,?,?,?,SYSDATE(),time(NOW()))`, ID, data.Name_site, data.Address_site, data.Manager_site, data.Manager_tel, filename, filepath)
	if err != nil {
		return err
	}
	return nil
}

func NewCreateReport() *Selected_group {
	return new(Selected_group)
}

func createReport(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	fmt.Println("*****createReport Handler running*****")
	w.Header().Set("Content-Type", "application/json")

	b, err := ioutil.ReadAll(req.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	var msg []Selected_group
	err = json.Unmarshal(b, &msg)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	fmt.Println(msg)
	db, _ := GetDB()
	defer db.Close()
	query := "INSERT INTO selected_group VALUES"
	var vals []interface{}

	for _, row := range msg {
		fmt.Println(row)
		query += "(NULL,?,?,?,?,?,?,?,SYSDATE(),time(NOW())) ,"                                                                              //<--- check param
		vals = append(vals, row.Id_user_sg, row.Id_site_sg, row.Id_group_selected, row.Id_ssop_selected, row.N_group, row.N_ssop, row.Score) //<---  check param
	}
	fmt.Println(vals)
	_, err = db.Exec(strings.TrimSuffix(query, " ,"), vals...) //<---check command insert
	fmt.Println(err)
}

//========================functions[GET]========================//
func account(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := prm.ByName("id")
	user, err := NewLogin().getaccount(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	error_check := json.NewEncoder(w).Encode(user)
	if error_check != nil {
		http.Error(w, error_check.Error(), http.StatusInternalServerError)
		return
	}
	if error_check == nil {
		fmt.Println("get acc. complete")
		return
	}
}

func (data *Log_in) getaccount(ID string) ([]Log_in, error) {
	users := make([]Log_in, 0)
	db, err := GetDB()
	if err != nil {
		return users, err
	}
	defer db.Close()
	rows, err := db.Query(`select Id_user, E_mail, Password , Up_date , Up_time from log_in where Id_user = ` + ID)
	if err != nil {
		return users, err
	}
	if rows.Next() {
		user := Log_in{}
		rows.Scan(&user.Id_user, &user.E_mail, &user.Password, &user.Up_date, &user.Up_time)
		users = append(users, user)
	}
	return users, err
}

func getsite(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := prm.ByName("id")
	siteId := prm.ByName("siteId")
	site, err := NewADDSite().getsitebysiteId(id, siteId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	error_check := json.NewEncoder(w).Encode(site)
	if error_check != nil {
		http.Error(w, error_check.Error(), http.StatusInternalServerError)
		return
	}
	if error_check == nil {
		fmt.Println("get site complete")
		return
	}
}

func (data *Site_test) getsitebysiteId(ID string, siteId string) ([]Site_test, error) {
	items := make([]Site_test, 0)
	db, err := GetDB()
	if err != nil {
		return items, err
	}
	defer db.Close()
	rows, err := db.Query(`select * from site_test where Id_user_test = ? and Id_site_test =  ?`, ID, siteId)
	if err != nil {
		return items, err
	}
	if rows.Next() {
		item := Site_test{}
		rows.Scan(&item.Id_site, &item.Id_user, &item.Name_site, &item.Address_site,
			&item.Manager_site, &item.Manager_tel, &item.File_name, &item.File_path,
			&item.Upd_date, &item.Upd_time)
		items = append(items, item)
	}
	return items, err
}

func getallsite(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := prm.ByName("id")
	items, err := NewADDSite().getallsitebyId(id)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	error_check := json.NewEncoder(w).Encode(items)
	if error_check != nil {
		log.Println(err.Error())
		http.Error(w, error_check.Error(), http.StatusInternalServerError)
		return
	}
	if error_check == nil {
		fmt.Println("get all site complete")
		return
	}
}

func (data *Site_test) getallsitebyId(id string) ([]Site_test, error) {
	items := make([]Site_test, 0)
	db, err := GetDB()
	if err != nil {
		return items, err
	}
	defer db.Close()
	rows, err := db.Query(`select * from site_test where Id_user_test = ?`, id)
	if err != nil {
		return items, err
	}
	for rows.Next() {
		item := Site_test{}
		rows.Scan(&item.Id_site, &item.Id_user, &item.Name_site, &item.Address_site,
			&item.Manager_site, &item.Manager_tel, &item.File_name, &item.File_path,
			&item.Upd_date, &item.Upd_time)
		items = append(items, item)
	}
	return items, nil
}

func getbubble(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := prm.ByName("id")
	bubbleId := prm.ByName("bubbleId")
	bubble, err := NewBubble().getbubblebybubbleId(id, bubbleId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	error_check := json.NewEncoder(w).Encode(bubble)
	if error_check != nil {
		http.Error(w, error_check.Error(), http.StatusInternalServerError)
		return
	}
	if error_check == nil {
		fmt.Println("get bubble complete")
		return
	}
}

func (data *Bubble) getbubblebybubbleId(ID string, bubbleId string) ([]Bubble, error) {
	items := make([]Bubble, 0)
	db, err := GetDB()
	if err != nil {
		return items, err
	}
	defer db.Close()
	rows, err := db.Query(`select * from bubble where Id_mng = ? and Id_bub =  ?`, ID, bubbleId)
	if err != nil {
		return items, err
	}
	if rows.Next() {
		item := Bubble{}
		rows.Scan(&item.Id_bub, &item.Id_mng, &item.Name_bub, &item.Detail_bub, &item.Date_bub, &item.Time_bub)
		items = append(items, item)
	}
	return items, err
}

func getallbubble(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := prm.ByName("id")
	items, err := NewBubble().getallbubblebyId(id)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	error_check := json.NewEncoder(w).Encode(items)
	if error_check != nil {
		log.Println(err.Error())
		http.Error(w, error_check.Error(), http.StatusInternalServerError)
		return
	}
	if error_check == nil {
		fmt.Println("get all bubble complete")
		return
	}
}

func (data *Bubble) getallbubblebyId(id string) ([]Bubble, error) {
	items := make([]Bubble, 0)
	db, err := GetDB()
	if err != nil {
		return items, err
	}
	defer db.Close()
	rows, err := db.Query(`select * from bubble where Id_mng = ?`, id)
	if err != nil {
		return items, err
	}
	for rows.Next() {
		item := Bubble{}
		rows.Scan(&item.Id_bub, &item.Id_mng, &item.Name_bub, &item.Detail_bub, &item.Date_bub, &item.Time_bub)
		items = append(items, item)
	}
	return items, nil
}

func NewGetWorker() *GetWorker {
	return new(GetWorker)
}

func getallworker(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := prm.ByName("id")
	items, err := NewGetWorker().getallworkerbyId(id)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	error_check := json.NewEncoder(w).Encode(items)
	if error_check != nil {
		log.Println(err.Error())
		http.Error(w, error_check.Error(), http.StatusInternalServerError)
		return
	}
	if error_check == nil {
		fmt.Println("get all worker complete")
		return
	}
}

func (data *GetWorker) getallworkerbyId(id string) ([]GetWorker, error) {
	items := make([]GetWorker, 0)
	db, err := GetDB()
	if err != nil {
		return items, err
	}
	defer db.Close()
	rows, err := db.Query("select *, (select b.Name_bub from bubble b where b.Id_bub=Id_bubble ) as Name_bubble from worker where Id_manager = ?", id)
	if err != nil {
		return items, err
	}
	for rows.Next() {
		item := GetWorker{}
		rows.Scan(&item.Id_worker, &item.Id_manager, &item.Id_bubble, &item.Name_worker, &item.Tel_worker,
			&item.Address_worker, &item.Date_worker, &item.Time_worker, &item.Name_bubble)
		items = append(items, item)
	}
	return items, nil
}

func getworker(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := prm.ByName("id")
	bubbleId := prm.ByName("bubbleId")
	items, err := NewWorker().getworkerbybubbleId(id, bubbleId)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	error_check := json.NewEncoder(w).Encode(items)
	if error_check != nil {
		log.Println(err.Error())
		http.Error(w, error_check.Error(), http.StatusInternalServerError)
		return
	}
	if error_check == nil {
		fmt.Println("get  worker complete")
		return
	}
}

func (data *Worker) getworkerbybubbleId(id string, bubbleId string) ([]Worker, error) {
	items := make([]Worker, 0)
	db, err := GetDB()
	if err != nil {
		return items, err
	}
	defer db.Close()
	rows, err := db.Query("select * from worker where Id_manager = ? and Id_bubble = ? order by Name_worker ASC", id, bubbleId)
	if err != nil {
		return items, err
	}
	for rows.Next() {
		item := Worker{}
		rows.Scan(&item.Id_worker, &item.Id_manager, &item.Id_bubble, &item.Name_worker, &item.Tel_worker,
			&item.Address_worker, &item.Date_worker, &item.Time_worker)
		items = append(items, item)
	}
	return items, nil
}

func getoneworker(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := prm.ByName("id")
	workerId := prm.ByName("workerId")
	worker, err := NewGetWorker().getworkerbybubbleId(id, workerId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	error_check := json.NewEncoder(w).Encode(worker)
	if error_check != nil {
		http.Error(w, error_check.Error(), http.StatusInternalServerError)
		return
	}
	if error_check == nil {
		fmt.Println("get spec. worker complete")
		return
	}
}

func (data *GetWorker) getworkerbybubbleId(ID string, workerId string) ([]GetWorker, error) {
	items := make([]GetWorker, 0)
	db, err := GetDB()
	if err != nil {
		return items, err
	}
	defer db.Close()
	rows, err := db.Query(`select *, (select b.Name_bub from bubble b where b.Id_bub=Id_bubble ) as Name_bubble from worker where Id_manager = ? and Id_worker =  ?`, ID, workerId)
	if err != nil {
		return items, err
	}
	if rows.Next() {
		item := GetWorker{}
		rows.Scan(&item.Id_worker, &item.Id_manager, &item.Id_bubble, &item.Name_worker, &item.Tel_worker, &item.Address_worker, &item.Date_worker, &item.Time_worker, &item.Name_bubble)
		items = append(items, item)
	}
	return items, err
}

type GetImgfilepath struct {
	Filepath string `json:"filepath"`
}

func NewGetImgfilepath() *GetImgfilepath {
	return new(GetImgfilepath)
}

type DataImg struct {
	Data string `json:"data"`
}

func Getimg(w http.ResponseWriter, r *http.Request, prm httprouter.Params) {
	imagepath := make([]GetImgfilepath, 0)
	if (*r).Method == "OPTIONS" {
		return
	}
	w.Header().Set("Content-Type", "application/json")
	ID := prm.ByName("id")
	db, err := GetDB()
	row, err := db.Query("select File_path_test from site_test where Id_user_test = ?", ID)
	fmt.Println(row)
	if err != nil {
		panic(err.Error())
	}
	for row.Next() {
		imgpath := GetImgfilepath{}
		row.Scan(&imgpath.Filepath)
		imagepath = append(imagepath, imgpath)
	}
	items := make([]DataImg, 0)
	defer row.Close()
	for _, row := range imagepath {
		bytes, _ := ioutil.ReadFile("/home/mew" + row.Filepath)
		var base64Encoding string
		// // Determine the content type of the image file
		mimeType := http.DetectContentType(bytes)
		switch mimeType {
		case "image/jpeg":
			base64Encoding += "data:image/jpeg;base64,"
		case "image/png":
			base64Encoding += "data:image/png;base64,"
		}
		base64Encoding += toBase64(bytes)
		fmt.Println(base64Encoding)
		user := DataImg{Data: base64Encoding}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusCreated)
		items = append(items, user)
	}
	json.NewEncoder(w).Encode(items)
}

func toBase64(b []byte) string {
	return base64.StdEncoding.EncodeToString(b)
}

type Getworkertoday struct {
	Name_worker    string `json:"Name_worker"`
	Tel_worker     string `json:"tel_worker"`
	Address_worker string `json:"address_worker"`
	Name_bubble    string `json:"name_bubble"`
}

func NewGetworkertoday() *Getworkertoday {
	return new(Getworkertoday)
}

func getworkertoday(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := prm.ByName("id")
	siteId := prm.ByName("siteId")
	worker, err := NewGetworkertoday().getworkertodayBysiteId(id, siteId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	error_check := json.NewEncoder(w).Encode(worker)
	if error_check != nil {
		http.Error(w, error_check.Error(), http.StatusInternalServerError)
		return
	}
	if error_check == nil {
		fmt.Println("get worker today complete")
		return
	}
}

func (data *Getworkertoday) getworkertodayBysiteId(ID string, siteId string) ([]Getworkertoday, error) {
	items := make([]Getworkertoday, 0)
	db, err := GetDB()
	if err != nil {
		return items, err
	}
	defer db.Close()
	rows, error_check := db.Query("SELECT Name_worker, Tel_worker, Address_worker, (select b.Name_bub from bubble b where b.Id_bub=worker.Id_bubble ) as Name_bubble FROM worker RIGHT JOIN selected_bubble ON worker.Id_bubble = selected_bubble.Id_bub_sb where worker.Id_manager= ? and selected_bubble.Id_site_sb= ? and selected_bubble.Date_sb = (SELECT DISTINCT CAST( sysdate()AS Date) as Date_select from selected_bubble) ORDER BY worker.Id_bubble ASC , worker.Name_worker ASC", ID, siteId)
	if error_check != nil {
		return items, error_check
	}
	for rows.Next() {
		item := Getworkertoday{}
		rows.Scan(&item.Name_worker, &item.Tel_worker, &item.Address_worker, &item.Name_bubble)
		items = append(items, item)
	}
	return items, error_check
}

func NewGetAllSsopByGroupId() *Ssop {
	return new(Ssop)
}

func getAllSsopByGroupId(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	groupId := prm.ByName("groupId")
	worker, err := NewGetAllSsopByGroupId().getAllSsopByGroupIdQR(groupId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	error_check := json.NewEncoder(w).Encode(worker)
	if error_check != nil {
		http.Error(w, error_check.Error(), http.StatusInternalServerError)
		return
	}
	if error_check == nil {
		fmt.Println("get worker today complete")
		return
	}
}

func (data *Ssop) getAllSsopByGroupIdQR(groupId string) ([]Ssop, error) {
	items := make([]Ssop, 0)
	db, err := GetDB()
	if err != nil {
		return items, err
	}
	defer db.Close()
	rows, error_check := db.Query("SELECT id_s, n_s,score_s from  group_of_ssop	where id_g = ?", groupId)
	if error_check != nil {
		return items, error_check
	}
	for rows.Next() {
		item := Ssop{}
		rows.Scan(&item.Id_ssop, &item.Description, &item.Score)
		items = append(items, item)
	}
	return items, error_check
}

func NewSsop_group() *Ssop_group {
	return new(Ssop_group)
}

func getGroupByGroupId(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	groupId := prm.ByName("groupId")
	worker, err := NewSsop_group().getGroupByGroupIdQR(groupId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	error_check := json.NewEncoder(w).Encode(worker)
	if error_check != nil {
		http.Error(w, error_check.Error(), http.StatusInternalServerError)
		return
	}
	if error_check == nil {
		fmt.Println("get 1 group complete")
		return
	}
}

func (data *Ssop_group) getGroupByGroupIdQR(groupId string) ([]Ssop_group, error) {
	items := make([]Ssop_group, 0)
	db, err := GetDB()
	if err != nil {
		return items, err
	}
	defer db.Close()
	rows, err := db.Query("SELECT distinct id_g, n_g from  group_of_ssop where id_g = ?", groupId)
	if err != nil {
		return items, err
	}
	if rows.Next() {
		item := Ssop_group{}
		rows.Scan(&item.Id_group, &item.Name_group)
		items = append(items, item)
	}
	return items, err
}

func NewSelected_bubble() *Selected_bubble {
	return new(Selected_bubble)
}

func getSelectSiteAndBubble(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := prm.ByName("id")
	siteId := prm.ByName("siteId")
	worker, err := NewSelected_bubble().getSelectSiteAndBubbleQR(id, siteId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	error_check := json.NewEncoder(w).Encode(worker)
	if error_check != nil {
		http.Error(w, error_check.Error(), http.StatusInternalServerError)
		return
	}
	if error_check == nil {
		fmt.Println("get get Select Site And Bubble complete")
		return
	}
}

func (data *Selected_bubble) getSelectSiteAndBubbleQR(id string, siteId string) ([]Selected_bubble, error) {
	items := make([]Selected_bubble, 0)
	db, err := GetDB()
	if err != nil {
		return items, err
	}
	defer db.Close()
	rows, error_check := db.Query("SELECT distinct Id_mng_sb, Id_site_sb, Id_bub_sb from  selected_bubble where Id_mng_sb = ? and Id_site_sb = ? and selected_bubble.Date_sb = (SELECT DISTINCT CAST( sysdate()AS Date) as date_select from selected_bubble)", id, siteId)
	if error_check != nil {
		return items, error_check
	}
	for rows.Next() {
		item := Selected_bubble{}
		rows.Scan(&item.Id_mng_sb, &item.Id_site_sb, &item.Id_bub_sb)
		items = append(items, item)
	}
	return items, error_check
}

func NewNotify() *Notify {
	return new(Notify)
}

func getDetection(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := prm.ByName("id")
	siteId := prm.ByName("siteId")
	worker, err := NewNotify().getDetectionQR(id, siteId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	error_check := json.NewEncoder(w).Encode(worker)
	if error_check != nil {
		http.Error(w, error_check.Error(), http.StatusInternalServerError)
		return
	}
	if error_check == nil {
		fmt.Println("get notify complete")
		return
	}
}

func (data *Notify) getDetectionQR(id string, siteId string) ([]Notify, error) {
	items := make([]Notify, 0)
	db, err := GetDB()
	if err != nil {
		return items, err
	}
	defer db.Close()
	rows, error_check := db.Query("SELECT date_lab,time_lab,name_result_person, result_person,name_result_hardhat,result_hardhat,name_result_nohardhat,result_nohardhat from lab where id_mng_lab = ? and id_site_lab= ? ORDER BY date_lab DESC , time_lab DESC", id, siteId)
	if error_check != nil {
		return items, error_check
	}
	for rows.Next() {
		item := Notify{}
		rows.Scan(&item.Date_lab, &item.Time_lab, &item.Name_result_person, &item.Result_lab_person, &item.Name_result_hardhat, &item.Result_lab_hardhat, &item.Name_result_nohardhat, &item.Result_lab_nohardhat)
		items = append(items, item)
	}
	return items, error_check
}

func NewSummary() *Summary {
	return new(Summary)
}

func getSummary(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := prm.ByName("id")
	siteId := prm.ByName("siteId")
	worker, err := NewSummary().getSummaryQR(id, siteId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	error_check := json.NewEncoder(w).Encode(worker)
	if error_check != nil {
		http.Error(w, error_check.Error(), http.StatusInternalServerError)
		return
	}
	if error_check == nil {
		fmt.Println("get summary complete")
		return
	}
}

func (data *Summary) getSummaryQR(id string, siteId string) ([]Summary, error) {
	items := make([]Summary, 0)
	db, err := GetDB()
	if err != nil {
		return items, err
	}
	defer db.Close()
	rows, error_check := db.Query("SELECT distinct date_lab,name_result_person, sum(result_person) as result_sum_person ,name_result_hardhat, sum(result_hardhat) as result_sum_hardhat,name_result_nohardhat,sum(result_nohardhat) as result_sum_nohardhat from lab where id_mng_lab = ? and id_site_lab= ? group by date_lab order by date_lab ASC", id, siteId)
	if error_check != nil {
		return items, error_check
	}
	for rows.Next() {
		item := Summary{}
		rows.Scan(&item.Date_lab, &item.Name_result_person, &item.Result_sum_person, &item.Name_result_hardhat, &item.Result_sum_hardhat, &item.Name_result_nohardhat, &item.Result_sum_nohardhat)
		items = append(items, item)
	}
	return items, error_check
}

type GetAllGroupAndSSOP struct {
	Id_g    int    `json:"id_g"`
	Id_s    int    `json:"id_s"`
	N_g     string `json:"n_g"`
	N_s     string `json:"n_s"`
	Score_s int    `json:"score_s"`
}

func NewGetAllGroupAndSSOP() *GetAllGroupAndSSOP {
	return new(GetAllGroupAndSSOP)
}

func getAllGroup(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	worker, err := NewGetAllGroupAndSSOP().getAllGroupQR()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	error_check := json.NewEncoder(w).Encode(worker)
	if error_check != nil {
		http.Error(w, error_check.Error(), http.StatusInternalServerError)
		return
	}
	if error_check == nil {
		fmt.Println("get getAllGroup complete")
		return
	}
}

func (data *GetAllGroupAndSSOP) getAllGroupQR() ([]GetAllGroupAndSSOP, error) {
	items := make([]GetAllGroupAndSSOP, 0)
	db, err := GetDB()
	if err != nil {
		return items, err
	}
	defer db.Close()
	rows, error_check := db.Query("SELECT id_g,id_s,n_g,n_s,score_s FROM group_of_ssop")
	if error_check != nil {
		return items, error_check
	}
	for rows.Next() {
		item := GetAllGroupAndSSOP{}
		rows.Scan(&item.Id_g, &item.Id_s, &item.N_g, &item.N_s, &item.Score_s)
		items = append(items, item)
	}
	return items, error_check
}

type GetAllLabByDay struct {
	Date_lab              string `json:"date_lab"`
	Name_result_person    string `json:"name_result_person"`
	Sum_person            int    `json:"sum_person"`
	Name_result_hardhat   string `json:"name_result_hardhat"`
	Sum_hardhat           int    `json:"sum_hardhat"`
	Name_result_nohardhat string `json:"name_result_ohardhat"`
	Sum_nohardhat         int    `json:"sum_nohardhat"`
}

func NewGetAllLabByDay() *GetAllLabByDay {
	return new(GetAllLabByDay)
}

type DataLab struct {
	Lab string `json:"lab"`
}

func getAllLabByDay(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := prm.ByName("id")
	siteId := prm.ByName("siteId")
	worker, err := NewGetAllLabByDay().getAllLabByDayQR(id, siteId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	error_check := json.NewEncoder(w).Encode(worker)
	if error_check != nil {
		http.Error(w, error_check.Error(), http.StatusInternalServerError)
		return
	}
	if error_check == nil {
		fmt.Println("get getAllLabByDay complete")
		return
	}
}

func (data *GetAllLabByDay) getAllLabByDayQR(id string, siteId string) ([]GetAllLabByDay, error) {
	items := make([]GetAllLabByDay, 0)
	db, err := GetDB()
	if err != nil {
		return items, err
	}
	defer db.Close()
	rows, error_check := db.Query("SELECT date_lab,name_result_person, sum(result_person) as sum_person,name_result_hardhat,sum(result_hardhat)as sum_hardhat,name_result_nohardhat,sum(result_nohardhat) as sum_nohardhat FROM ccc_database.lab where id_mng_lab= ? and id_site_lab=? GROUP BY date_lab order by date_lab asc", id, siteId)
	if error_check != nil {
		return items, error_check
	}
	for rows.Next() {
		item := GetAllLabByDay{}
		rows.Scan(&item.Date_lab, &item.Name_result_person, &item.Sum_person, &item.Name_result_hardhat, &item.Sum_hardhat, &item.Name_result_nohardhat, &item.Sum_nohardhat)
		items = append(items, item)
	}
	return items, error_check
}

type GetLabByMonth struct {
	Month                 string `json:"month"`
	Name_result_person    string `json:"name_result_person"`
	Sum_person            int    `json:"sum_person"`
	Name_result_hardhat   string `json:"name_result_hardhat"`
	Sum_hardhat           int    `json:"sum_hardhat"`
	Name_result_nohardhat string `json:"name_result_nohardhat"`
	Sum_nohardhat         int    `json:"sum_nohardhat"`
}

func NewGetLabByMonth() *GetLabByMonth {
	return new(GetLabByMonth)
}

func getLabByMonth(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := prm.ByName("id")
	siteId := prm.ByName("siteId")
	worker, err := NewGetLabByMonth().getLabByMonthQR(id, siteId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	error_check := json.NewEncoder(w).Encode(worker)
	if error_check != nil {
		http.Error(w, error_check.Error(), http.StatusInternalServerError)
		return
	}
	if error_check == nil {
		fmt.Println("get getLabByMonth complete")
		return
	}
}

func (data *GetLabByMonth) getLabByMonthQR(id string, siteId string) ([]GetLabByMonth, error) {
	items := make([]GetLabByMonth, 0)
	db, err := GetDB()
	if err != nil {
		return items, err
	}
	defer db.Close()
	rows, error_check := db.Query("SELECT DATE_FORMAT(date_lab, '%M, %Y')as 'Month',name_result_person, sum(result_person),name_result_hardhat,sum(result_hardhat),name_result_nohardhat,sum(result_nohardhat) FROM ccc_database.lab where id_mng_lab=? and id_site_lab=? GROUP BY MONTH(date_lab)", id, siteId)
	if error_check != nil {
		return items, error_check
	}
	for rows.Next() {
		item := GetLabByMonth{}
		rows.Scan(&item.Month, &item.Name_result_person, &item.Sum_person, &item.Name_result_hardhat, &item.Sum_hardhat, &item.Name_result_nohardhat, &item.Sum_nohardhat)
		items = append(items, item)
	}
	return items, error_check
}

type GetReport struct {
	G     string `json:"g"`
	Score int    `json:"score"`
}

func NewGetReport() *GetReport {
	return new(GetReport)
}

func (data *GetReport) getReportQR(ID string, siteId string) ([]GetReport, error) {
	users := make([]GetReport, 0)
	db, err := GetDB()
	if err != nil {
		return users, err
	}
	defer db.Close()
	rows, err := db.Query(`select distinct (select distinct n_g from group_of_ssop where id_g=1) g, 
	ifnull((select sum(score) from selected_group where id_group_selected= 1 and Date_sg = (select max(Date_sg) from selected_group where Id_user_sg= ` + ID + ` and Id_site_sg= ` + siteId + `) 
	and Time_sg = (select max(Time_sg) from selected_group where Date_sg = (select max(Date_sg) from selected_group where Id_user_sg= ` + ID + ` and Id_site_sg= ` + siteId + `)) and Id_user_sg= ` + ID + ` and Id_site_sg= ` + siteId + ` GROUP BY Date_sg ),0) score
	from selected_group
	union all
	select distinct (select distinct n_g from group_of_ssop where id_g=2) g, 
	ifnull((select sum(score) from selected_group where id_group_selected=2 and Date_sg = (select max(Date_sg) from selected_group where Id_user_sg= ` + ID + ` and Id_site_sg= ` + siteId + `) 
	and Time_sg = (select max(Time_sg) from selected_group where Date_sg = (select max(Date_sg) from selected_group where Id_user_sg= ` + ID + ` and Id_site_sg= ` + siteId + `)) and Id_user_sg= ` + ID + ` and Id_site_sg= ` + siteId + ` GROUP BY Date_sg ),0) score
	from selected_group
	union all
	select distinct (select distinct n_g from group_of_ssop where id_g=3) g, 
	ifnull((select sum(score) from selected_group where id_group_selected=3 and Date_sg = (select max(Date_sg) from selected_group where Id_user_sg= ` + ID + ` and Id_site_sg= ` + siteId + `) 
	and Time_sg = (select max(Time_sg) from selected_group where Date_sg = (select max(Date_sg) from selected_group where Id_user_sg= ` + ID + ` and Id_site_sg= ` + siteId + `)) and Id_user_sg= ` + ID + ` and Id_site_sg= ` + siteId + ` GROUP BY Date_sg ),0) score
	from selected_group
	union all
	select distinct (select distinct n_g from group_of_ssop where id_g=4) g, 
	ifnull((select sum(score) from selected_group where id_group_selected=4 and Date_sg = (select max(Date_sg) from selected_group where Id_user_sg= ` + ID + ` and Id_site_sg= ` + siteId + `) 
	and Time_sg = (select max(Time_sg) from selected_group where Date_sg = (select max(Date_sg) from selected_group where Id_user_sg= ` + ID + ` and Id_site_sg= ` + siteId + `) ) and Id_user_sg= ` + ID + ` and Id_site_sg= ` + siteId + ` GROUP BY Date_sg ),0) score
	from selected_group`)
	if err != nil {
		return users, err
	}
	for rows.Next() {
		user := GetReport{}
		rows.Scan(&user.G, &user.Score)
		users = append(users, user)
	}
	return users, err
}

func getReport(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := prm.ByName("id")
	siteId := prm.ByName("siteId")
	worker, err := NewGetReport().getReportQR(id, siteId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	error_check := json.NewEncoder(w).Encode(worker)
	if error_check != nil {
		http.Error(w, error_check.Error(), http.StatusInternalServerError)
		return
	}
	if error_check == nil {
		fmt.Println("get getReport complete")
		fmt.Println(worker)
		return
	}
}

type GetReportSSOP struct {
	Id_ssop   int    `json:"id_ssop"`
	Name_ssop string `json:"name_ssop"`
	Score     int    `json:"score"`
}

func NewGetReportSSOP() *GetReportSSOP {
	return new(GetReportSSOP)
}

func (data *GetReportSSOP) getReportSSOPQR(ID string, siteId string) ([]GetReportSSOP, error) {
	users := make([]GetReportSSOP, 0)
	db, err := GetDB()
	if err != nil {
		return users, err
	} //  ` + ID + ` and Id_site_sg= ` +siteId +
	defer db.Close()
	rows, err := db.Query(`select distinct id_s as id_ssop,n_s as name_ssop,
	ifnull((select score from selected_group where id_ssop_selected=id_s and Date_sg = (select max(Date_sg) from selected_group where Id_user_sg= ` + ID + ` and Id_site_sg= ` + siteId + ` ) and Time_sg = (select max(Time_sg) from selected_group where Date_sg = (select max(Date_sg) from selected_group where Id_user_sg=` + ID + ` and Id_site_sg= ` + siteId + ` ))),0) as score
	from selected_group, group_of_ssop
	where Date_sg = (select max(Date_sg) from selected_group where Id_user_sg= ` + ID + ` and Id_site_sg= ` + siteId + ` ) and Time_sg = (select max(Time_sg) from selected_group where Date_sg = (select max(Date_sg) from selected_group where Id_user_sg= ` + ID + ` and Id_site_sg= ` + siteId + ` ))`)
	if err != nil {
		return users, err
	}
	for rows.Next() {
		user := GetReportSSOP{}
		rows.Scan(&user.Id_ssop, &user.Name_ssop, &user.Score)
		users = append(users, user)
	}
	return users, err
}

func getReportSSOP(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := prm.ByName("id")
	siteId := prm.ByName("siteId")
	worker, err := NewGetReportSSOP().getReportSSOPQR(id, siteId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	error_check := json.NewEncoder(w).Encode(worker)
	if error_check != nil {
		http.Error(w, error_check.Error(), http.StatusInternalServerError)
		return
	}
	if error_check == nil {
		fmt.Println("get getReportSSOP complete")
		fmt.Println(worker)
		return
	}
}

//========================functions[UPDATE]========================//
func (data *Worker) Update(newWorker Worker) error {
	db, err := GetDB()
	if err != nil {
		return err
	}
	_, err = db.Exec(`UPDATE worker	SET Id_bubble = ?, Name_worker = ?, Tel_worker = ?, Address_worker = ?, Date_worker = sysdate(), Time_worker = time(now())
	WHERE Id_worker = ?`, newWorker.Id_bubble, newWorker.Name_worker, newWorker.Tel_worker, newWorker.Address_worker, newWorker.Id_worker)
	if err != nil {
		return err
	}
	return nil
}

func UpdateWorker(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	var warehouse Worker
	err := json.NewDecoder(req.Body).Decode(&warehouse)
	defer req.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = NewWorker().Update(warehouse)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err == nil {
		fmt.Println("update  worker complete")
		return
	}
}

func (data *Bubble) UpdateBub(newWorker Bubble) error {
	db, err := GetDB()
	if err != nil {
		return err
	}
	_, err = db.Exec(`UPDATE bubble
	SET Name_bub = ?, Detail_bub = ?, Date_bub = sysdate(), Time_bub = time(now())
	WHERE Id_bub = ?`, newWorker.Name_bub, newWorker.Detail_bub, newWorker.Id_bub)
	if err != nil {
		return err
	}
	return nil
}

func UpdateBubble(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	var warehouse Bubble
	err := json.NewDecoder(req.Body).Decode(&warehouse)
	defer req.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = NewBubble().UpdateBub(warehouse)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err == nil {
		fmt.Println("update  bubble complete")
		return
	}
}

func (data *SiteUpdate) UpdateSite(newSite SiteUpdate) error {
	db, err := GetDB()
	if err != nil {
		return err
	}
	_, err = db.Exec("UPDATE site_test SET Name_site_test = ?, Address_site_test = ?, Manager_site_test = ?, Manager_tel_test = ?,Upd_date_test = sysdate(), Upd_time_test = time(now()) WHERE Id_site_test = ? and Id_user_test = ? ",
		newSite.Name_site, newSite.Address_site, newSite.Manager_site, newSite.Manager_tel, newSite.Id_site, newSite.Id_user)
	if err != nil {
		return err
	}
	return nil
}

type SiteUpdate struct {
	Id_site      int    `json:"id_site"`
	Id_user      int    `json:"id_user"`
	Name_site    string `json:"name_site"`
	Address_site string `json:"address_site"`
	Manager_site string `json:"manager_site"`
	Manager_tel  string `json:"manager_tel"`
}

func NewSiteUpdate() *SiteUpdate {
	return new(SiteUpdate)
}

func updateSite(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	ID := prm.ByName("id")
	siteId := prm.ByName("siteId")
	w.Header().Set("Content-Type", "multipart/form-data")
	body := req.Body
	var site SiteUpdate
	site.Name_site = req.FormValue("name_site")
	site.Address_site = req.FormValue("address_site")
	site.Manager_site = req.FormValue("manager_site")
	site.Manager_tel = req.FormValue("manager_tel")
	defer body.Close()

	site.Id_site, _ = strconv.Atoi(siteId)
	site.Id_user, _ = strconv.Atoi(ID)
	fmt.Println(site)
	db, err := GetDB()
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()
	err = NewSiteUpdate().UpdateSite(site)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err == nil {
		fmt.Println("update site complete")
		fmt.Println(site)
		return
	}
}

//========================functions[DELETE]========================//
func (data *Worker) DeleteWorker(workerId string) error {
	db, err := GetDB()
	if err != nil {
		return err
	}
	_, err = db.Exec(`DELETE FROM worker WHERE Id_worker = ?`, workerId)
	if err != nil {
		return err
	}
	return nil
}

func DelWorker(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	workerId := prm.ByName("workerId")
	fmt.Println(workerId)
	err := NewWorker().DeleteWorker(workerId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err == nil {
		fmt.Println("delete worker complete")
		return
	}
}

func (data *Bubble) DeleteBubble(bubbleId string) error {
	db, err := GetDB()
	if err != nil {
		return err
	}
	_, err = db.Exec(`call delBubble(?)`, bubbleId)
	if err != nil {
		return err
	}
	return nil
}

func DelBubble(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	bubbleId := prm.ByName("bubbleId")
	fmt.Println(bubbleId)
	err := NewBubble().DeleteBubble(bubbleId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err == nil {
		fmt.Println("delete bubble complete")
		return
	}
}

//========================router========================//
func NewRouter() *httprouter.Router {
	router := httprouter.New()
	router.POST("/register", register)                 // สมัคร
	router.POST("/login", login)                       // เข้าสู่ระบบ
	router.POST("/logout", logout)                     // ออกจากระบบ
	router.POST("/addbubble/:id", add_bubble)          // เพิ่มบับเบิ้ล
	router.POST("/addworker/:id", add_worker)          // เพิ่มคนงาน
	router.POST("/addselectedbubble", selected_bubble) // เลือกบับเบิ้ลเข้าไซต์งาน เลือกได้หลายบับเบิ้ล **หน้าบ้านจัดการกับการเลือก**
	router.POST("/uploadsite/:id", uploadsite)         // รวมรูป + form add site ******ลองใช้อันนี้ก่อน*******
	router.GET("/index/:id/:siteId", Index)            // check
	router.GET("/about", aboutHandler)                 // check
	router.GET("/account/:id", account)                // ข้อมูลผู้ใช้งาน 1 คน
	router.GET("/getbubble/:id/:bubbleId", getbubble)  // ข้อมูลบิบเบิ้ล 1 บับเบิ้ล
	router.GET("/getallbubble/:id", getallbubble)      // ข้อมูลบับเบิ้ลทั้งหมด
	router.GET("/getallworker/:id", getallworker)
	router.GET("/getworker/:id/:bubbleId", getworker)                         //ข้อมูลคนงานทั้งหมด BY bubble id
	router.PUT("/updateworker", UpdateWorker)                                 //อัพเดทข้อมูลคนงาน
	router.PUT("/updatebubble", UpdateBubble)                                 //อัพเดทข้อมูลบับเบิ้ล
	router.DELETE("/delworker/:workerId", DelWorker)                          //ลบข้อมูลคนงาน
	router.DELETE("/delbubble/:bubbleId", DelBubble)                          //ลบข้อมูลบับเบิ้ล
	router.GET("/getoneworker/:id/:workerId", getoneworker)                   //ข้อมูลคนงาน 1 คน
	router.GET("/getimg/:id", Getimg)                                         //ดึงรูป ****อย่าลืมสร้าง class รับ data img  type DataImg struct { Data string `json:"data"`}
	router.GET("/getsite/:id/:siteId", getsite)                               // ข้อมูลไซต์งาน 1 ไซต์
	router.GET("/getallsite/:id", getallsite)                                 // ข้อมูลไซต์งานทั้งหมด
	router.GET("/getworkertoday/:id/:siteId", getworkertoday)                 // get * ข้อมูลคนงาน มีหลายบับเบิ้ล ใน table selected_bubble order by date
	router.GET("/getssop/:groupId", getAllSsopByGroupId)                      //get all id_s(id ssop) by id_g(id group)=1 in table group_of_ssop send data => id_s, n_s, score_s
	router.GET("/getgroup/:groupId", getGroupByGroupId)                       // get 1 group send data =>id_g,n_g
	router.GET("/getselectsiteandbubble/:id/:siteId", getSelectSiteAndBubble) // get Id_mng_sb,Id_site_sb,Id_bub_sb where site_Id=siteId เก็ทไปพร้อมกับปุ่มsubmitตอนเลือกบับเบิ้ลเข้าไซต์งาน เผื่อหน้าบ้านจะเอาข้อมูลบับเบิ้ล +ไซต์ไปใช้ทีหลัง
	router.POST("/createreport", createReport)
	router.GET("/notify/:id/:siteId", getDetection)   // get all and add Name_result, table = lab
	router.GET("/summary/:id/:siteId", getSummary)    // get all and sum by date, table= lab
	router.PUT("/updatesite/:id/:siteId", updateSite) // checkกับบิว
	router.GET("/getallgroupandssop", getAllGroup)    //Get ALL in  group_of_ssop
	router.GET("/getalllabbyday/:id/:siteId", getAllLabByDay)
	router.GET("/getlabbymonth/:id/:siteId", getLabByMonth)
	router.GET("/getreport/:id/:siteId", getReport)
	router.GET("/getreportssop/:id/:siteId", getReportSSOP) //new
	router.ServeFiles("/images/*filepath", http.Dir("images"))

	return router
}

//========================database========================//

const (
	DBDriver   = "mysql"
	DBName     = "ccc_database"
	DBUser     = "root"
	DBPassword = "1q2w3e4r"
	DBURL      = DBUser + ":" + DBPassword + "@tcp(10.101.118.233:9906)/" + DBName
)

func GetDB() (*sql.DB, error) {
	db, err := sql.Open(DBDriver, DBURL)
	if err != nil {
		return db, err
	}
	err = db.Ping()
	if err != nil {
		return db, err
	}
	return db, nil
}

func main() {
	log.Println("Server is up on 9000 port")
	router := NewRouter()
	headersOk := handlers.AllowedHeaders([]string{"Content-Type", "Accept", "Content-Length", "Authorization"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"OPTIONS", "DELETE", "GET", "HEAD", "POST", "PUT", "PATCH"})
	log.Fatalln(http.ListenAndServe(":9000", handlers.CORS(originsOk, headersOk, methodsOk)(router)))

}
